import java.net.*;
import java.util.HashMap;
import java.util.Map;

/** Stores the state of all current connections and manages them. */
//Could possible use refactoring? Seems to be doing multiple jobs...
//Seems to be too complicated. Second reason to refactor.
public class ConnectionManager {	
	private Netchat chat;
	private ServerListener connListener;
	private HashMap<String, Connection> conns = new HashMap<String, Connection>();
	private boolean connected = false;
	
	public ConnectionManager(Netchat netchat) {
		this.chat = netchat;
		this.connListener = new ServerListener(this);
		//Instantiate a connection listener and start it running.
		Thread listenThread = new Thread(connListener);
		listenThread.start();
	}

	/** Opens a new connection to the specified IP address (v4) */
	public void newConn(String ip) {
		System.out.println("|"+ip+"|");
		Connection c = new Connection(this, ip);
		if(c.openConnection(ip)){
			conns.put(ip, c);
		}
	}

	/** Add a socket to the manager, mapped to id */
	//refactor ID system. Refactor entire method.
	protected void addConn(String id, Socket sock) {
		Connection c = new Connection(this, id);
		if(c.openConnection(sock)){
			conns.put(id, c);
		}
	}

	/** Attempts to close a connection then forgets it. */
	public void dropConn(String id) {
		Connection toClose = conns.get(id);
		if(toClose != null) {
			toClose.close();
		}
	}

	public void write(String mesg) {
		for(Map.Entry<String,Connection> entry : conns.entrySet()) {
			Connection c = entry.getValue();
			//System.out.println("|"+c.getId()+"|");
			c.write(mesg);
		}
	}

	/** Called when a connection closes of its own accord */
	protected void onClose(String id) {
		conns.remove(id);
	}

	public int numConns() {
		return conns.size();
	}

	protected Netchat getChat() {
		return chat;
	}
}
