import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

class CommandReader {
	BufferedReader buffer;
	public CommandReader() {
		InputStreamReader streamRead = new InputStreamReader(System.in);
		buffer = new BufferedReader(streamRead);
	}

	private void onReadError() {
		System.err.println("An IO Exception has occurred in a console reader!");
	}

	public String readLine(boolean blocking) {
		try {
			if(buffer.ready() || blocking) {
				//Can't return directly as it ceases being blocking. :(
				String read = buffer.readLine();
				return read;
			}

		} catch(IOException e) {
			onReadError();
		}

		return null;
	}
}
