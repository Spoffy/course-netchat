import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;

class ConnOutput {
	private OutputStream out;
	private Connection conn;

	public ConnOutput(Connection newConn) {
		this.conn = newConn;
		try {
			out = conn.getSock().getOutputStream();
		} catch(IOException e) {
			System.out.println("IO Exception when opening output connection.");
			conn.onOutputError();
		}
	}

	public void write(String mesg) {
		//Should be a non blocking call, else a new thread will be needed.
		try {
			out.write(mesg.getBytes("ASCII"));
		} catch(IOException e) {
			System.out.println("Error while outputting messages.");
			conn.onOutputError();
		}
	}
}
	
