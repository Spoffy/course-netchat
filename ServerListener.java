import java.net.*;
import java.io.IOException;

/**Class listens for incoming connections and passes them on to the connection manager. */
class ServerListener implements Runnable {
	private ConnectionManager connMngr;
	private ServerSocket servSock;
	/**Configurable port to listen on. */
	private final int port = 9001;

	/**Get connection manager we're pushing to, open a server socket.*/
	public ServerListener (ConnectionManager connMngr) {
		this.connMngr = connMngr;
		try {
			servSock = new ServerSocket(port);
		} catch(IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/**Called by Thread. Run this to set the Listener to continously hunt connections. */
	public void run() {
		boolean running = true;
		//Continuously loop to keep keep hunting for connections.
		while(running) {
			//Listening for a new connection. On recieving one, pass it on to the connection manager.
			System.out.println("Listening");
			try {
				Socket currConn = servSock.accept();
				String ip = currConn.getInetAddress().getHostAddress();
				connMngr.addConn(ip, currConn);
			} catch(IOException e) {
				throw new RuntimeException("IO Exception while waiting for connection.");
			}
		}
	}
}
