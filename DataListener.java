import java.net.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/** Listens on a connection for incoming messages, and posts them back to the chat application. */
class DataListener implements Runnable {
	private Socket sock;
	private Connection conn;
	private BufferedReader input;

	public DataListener(Connection conn) {
		this.conn = conn;
		this.sock = conn.getSock();
	}

	public void run() {
		try {
			boolean listening = true;
			input = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			while(listening) {
				//Blocking call.
				String data = input.readLine();
				conn.receiveData(data);
			}
		} catch(IOException e) {
			//Refactor?
			System.out.println("Closing a connection");
			//Connection isn't able to IO anymore, probably doesn't work, should close it. Is this good code (As Exception is part of program flow)?
			conn.onInputError();
			return;
		}
	}
}

