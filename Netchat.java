import java.util.LinkedList;
import java.net.Socket;

/**Application class - Main program logic.*/
//HEAVY WIP
public class Netchat {
	public ConnectionManager connMngr;
	public CommandReader cmdReader;
	public LinkedList<String> recievedMessages = new LinkedList<String>();

	public static void main(String [] args) {
		Netchat program = new Netchat();

		//program.connMngr.newConn("127.0.0.1");
		//program.connMngr.write("Testing 1234\n");
		while(true) {
			try {
				String input = program.cmdReader.readLine(true);	
				if(input != null) {
					program.interpret(input);
				}
				Thread.sleep(1);
			} catch(Exception e) {}
		}
		
	}

	public Netchat() {
		connMngr = new ConnectionManager(this);
		cmdReader = new CommandReader();
	}

	public void write(String text){
		connMngr.write(text + "\n");
	}

	//DEBUGGING FOR NOW
	public void pushMessage(String id, String mesg) {
		mesg = "[" + id + "]: " + mesg;
		System.out.println(mesg);
		recievedMessages.add(mesg);
	}

	public void interpret(String input) {
		if(input.length() == 0) {return;};
		if(!input.substring(0,1).equals("/")) {
			write(input);
		}

		if(input.matches("/connect \\d+\\.\\d+\\.\\d+\\.\\d+")) {
			connMngr.newConn(input.substring(9));
		}
	}
}
