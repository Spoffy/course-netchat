import java.net.*;
import java.io.IOException;
import java.io.OutputStream;

class Transmitter implements Runnable {
	Socket currConn;

	public Transmitter() {
		currConn = new Socket();
	}

	public void run() {
		InetSocketAddress addr = new InetSocketAddress(InetAddress.getLoopbackAddress(), 9001);
		OutputStream output;
		try {
			currConn.connect(addr);
			output = currConn.getOutputStream();
		} catch(IOException e) {
			System.out.println("Unable to connect to " + addr.getHostString() + " IO exception thrown.");
			return;
		}

		while(true) {
			try {
				System.out.println("Writing.");
				output.write("Boomofo\n".getBytes("ASCII"));
				Thread.sleep(1000);
			} catch(IOException e) {
				System.out.println("One");
			} catch(InterruptedException e) {
				System.out.println("Two");
			}
		}
	}
}
