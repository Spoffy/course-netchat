import java.net.*;
import java.io.IOException;

/**Represents an active connection, acts as a wrapper for DataListener and ConnOutput */
class Connection {
	private final String id;
	private ConnectionManager manager;
	private Socket sock;
	private DataListener dataListener;
	private ConnOutput output;
	private boolean ready = false;

	public Connection(ConnectionManager connMan, String newId) {
		manager = connMan;
		id = newId;
	}

	public boolean openConnection(String ip) {
		ready = true;
		sock = new Socket();
		InetSocketAddress addr = new InetSocketAddress(ip, 9001);
		try {
			sock.connect(addr);
		} catch(IOException e) {
			System.out.println("Unable to connect.");
			close();
		}
		//End
		this.finalConnInit();
		return ready;
	}

	public boolean openConnection(Socket newSock) {
		ready = true;
		sock = newSock;
		this.finalConnInit();
		return ready;
	}

	private void finalConnInit() {
		dataListener = new DataListener(this);
		Thread listenThread = new Thread(dataListener);
		listenThread.start();
		output = new ConnOutput(this);
	}

	protected Socket getSock() {
		return sock;
	}

	public String getId() {
		return id;
	}

	protected void onInputError() {
		close();
	}

	protected void onOutputError() {
		close();
	}

	//Refactor this to use a message control thing.
	/** Called when some data is received on the connection by the DataListener. */
	protected void receiveData(String data) {
		manager.getChat().pushMessage(id, data);	
	}

	/** Writes a string to the connection output. */
	public void write(String mesg) {
		output.write(mesg);
	}

	/** Closes down the connection. */
	public void close() {
		try {
			sock.close();
		} catch(IOException e) {
			System.out.println("Error closing socket in Connection.close()");
		}
		ready = false;
		manager.onClose(id);
	}
}

